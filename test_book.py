""" Tests for book functionallity"""

import copy
from collections import namedtuple
from mocks import inventory
from library.item.book import Book
from library.inventory.inventory import Inventory

BookAttrs = namedtuple(
    "BookAttrs",
    ["title", "isbn", "given_id", "pages", "publisher", "published_year", "authors"]
)

BOOK_VALUES = BookAttrs(
    "The Emperor's New Clothes",
    "978-0-441-11773-4",
    "0",
    48,
    "Houghton Mifflin Books for Children ",
    2004,
    ["Hans Christian Andersen"]
)


def make_test_inventory() -> Inventory:
    """creates an already filled iventory with dummy data"""

    return copy.deepcopy(inventory)


def test_is_a_book():
    """tests is possible to create books"""
    book = Book(BOOK_VALUES.given_id)

    assert book is not None


def test_book_has_attrs():
    """tests the book has needed attributes"""
    book = Book(BOOK_VALUES.given_id)
    book.title = BOOK_VALUES.title
    book.isbn = BOOK_VALUES.isbn
    book.pages = BOOK_VALUES.pages
    book.publisher = BOOK_VALUES.publisher
    book.published_year = BOOK_VALUES.published_year
    book.authors.append(BOOK_VALUES.authors)

    assert book.title is not None
    assert book.authors is not None
    assert book.pages is not None
    assert book.publisher is not None
    assert book.published_year is not None


def test_books_build_from_str_info():
    """tests books can be created from a string"""
    books_raw_info = """Book:
    Author: Brian Jensen
    Title: Texts from Denmark
    Publisher: Gyldendal
    Published: 2001
    
    Book:
    Author: Peter Jensen
    Author: Hans Andersen
    Title: Stories from abroad
    Publisher: Borgen
    Published: 2012"""
    #removes tabs
    books_raw_info = books_raw_info.replace("\n    ", "\n")

    books = Book.read_books(books_raw_info)

    assert len(books) == 2


def test_find_books():
    """tests a book is discoverable through a string"""
    test_inventory = make_test_inventory()

    search_equals_to = "Gyldendal"
    search_inits_with = "20*"
    search_ends_with = "*sen"
    search_has = "*20*"
    search_on_two_conditions = "*20* & *Han*"
    search_on_three_conditions = "*20* & *Ab* & Planeta"
    search_for_no_book = "*20* & this libro that no exists"
    search_on_or_condition = "Gyldendal | Planeta"
    search_with_mixed_condition = "Gyldendal | *Han* & *20*"

    #searching existing books
    assert len(test_inventory.find_books(search_equals_to)) == 1
    assert len(test_inventory.find_books(search_inits_with)) == 3
    assert len(test_inventory.find_books(search_ends_with)) == 2
    assert len(test_inventory.find_books(search_has)) == 3
    assert test_inventory.find_books(search_on_three_conditions)[0].given_id == "2"
    assert len(test_inventory.find_books(search_on_two_conditions)) > 0
    assert len(test_inventory.find_books(search_on_or_condition)) == 2
    assert len(test_inventory.find_books(search_with_mixed_condition)) == 2

    #searching not existing books
    assert len(test_inventory.find_books(search_for_no_book)) == 0


def test_retrieve_books_of_row():
    """tests the retrieval of books in a row"""
    test_inventory = make_test_inventory()

    assert len(test_inventory.get_books_from("row", "0")) == 1
    assert len(test_inventory.get_books_from("row", "1")) == 2
    assert len(test_inventory.get_books_from("row", "2")) == 0


def test_retrieve_books_of_shelf():
    """tests the retrieval of books in a shelf"""
    test_inventory = make_test_inventory()

    assert len(test_inventory.get_books_from("shelf", "0")) == 1
    assert len(test_inventory.get_books_from("shelf", "1")) == 2
    assert len(test_inventory.get_books_from("shelf", "2")) == 0


def test_retrieve_books_of_room():
    """tests the retrieval of books in a shelf"""
    test_inventory = make_test_inventory()

    assert len(test_inventory.get_books_from("room", "0")) == 1
    assert len(test_inventory.get_books_from("room", "1")) == 2
    assert len(test_inventory.get_books_from("room", "2")) == 0


def test_locate_books():
    """tests the locating of a book in the library"""
    test_inventory = make_test_inventory()

    assert test_inventory.locate("xxx-xx-xxx-xz") == "room: 1 -> shelf: 1 -> row: 1"
    assert test_inventory.locate("xxx-xx-xxx-xy") == "room: 1 -> shelf: 1 -> row: 1"
    assert test_inventory.locate("xxx-xx-xxx-xx") == "room: 0 -> shelf: 0 -> row: 0"
