# Milestone Library
Implementation of a library management system build under **python 3.8.2** version

Usage:
* install `pipenv` tool
* run `pipenv install`
* run `pipenv run pytest` in order to runn the test cases

Recomended the usage of the tool `pyenv` to allow multiple python versions in the same machine with no conficts working flawlessly with `pipenv`

## Coding process
For the codig process it was used the TDD methodologie (you can see the branch `read-book-feature` which depicts better the process). The merge strategy was `rebase`, branches folowing `git-flow` like style are kept alive to see better the process.

The next image shows the design for the classes followed for the exercise

![class diagram](https://www.dropbox.com/s/1omclaa1m4mzzut/milestone_library.png "class diagram")

**Eduardo Galeano 2021**