"""Physical class"""

from .item import Item


class Physical(Item):
    """represents a physical library item"""
