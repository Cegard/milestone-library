"""Item class"""


class Item:
    """represents a library item"""


    def __init__(self, given_id: str):
        self.title = ""
        self.isbn = ""
        self.given_id = given_id
    