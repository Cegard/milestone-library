"""Book class"""

from typing import List
from .helpers import update_id
from .physical import Physical


def make_book_field(field: str):
    """creates a function that gives the value
    to the specified field of the given book
    """
    function = lambda book, value: book.__dict__.update({field: value})

    if field == "authors":
        function = lambda book, value: book.authors.append(value)

    elif field in ["published_year"]:
        function = lambda book, value: book.__dict__.update({field: int(value)})

    return function


RULES = {
    "Author": make_book_field("authors"),
    "Title": make_book_field("title"),
    "Publisher": make_book_field("published"),
    "Published": make_book_field("published_year")
}

DUMMY_RULE = lambda book, value: None


class Book(Physical):
    """represents a library physical book"""


    def __init__(self, given_id):
        super().__init__(given_id)
        self.pages: int
        self.publisher: str
        self.published_year: int
        self.authors: List[str] = []


    @staticmethod
    def read_books(text_input: str) -> list:
        """creates a list of books from the given text_input"""
        readed_books = []
        info_blocks = text_input.split("\n\n")

        for block in info_blocks:
            aux_book = Book(update_id("Book"))
            info = block.split("\n")

            for line in info:
                instruction = line.split(": ") + [""]
                RULES.get(instruction[0], DUMMY_RULE)(aux_book, instruction[1])

            readed_books.append(aux_book)

        return readed_books
