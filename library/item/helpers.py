"""helper functions for item package"""

__IDS = dict()


def update_id(class_name: str) -> int:
    """increments by one the id for the class and returns its new value"""
    __IDS[class_name] = __IDS.get(class_name, 0) + 1

    return __IDS[class_name]


def get_id(class_name: str) -> int:
    """retrieves the current id"""

    return __IDS.get(class_name, 0)
