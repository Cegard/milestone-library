"""FromRowRetriever class"""

from library.retriever.helpers import get_books, locate_the_book
from typing import List
from library.item.book import Book
from .retrieve_behavior import RetrieveBehavior
from .row import Row


class FromRowRetriever(RetrieveBehavior):
    """implements the RretrieveBhavior 'interface' to look fro books on rows"""


    def __init__(self) -> None:
        super().__init__()
        self.rows: List[Row] = []


    def retrieve_books(self, container_id: str) -> List[Book]:

        return get_books(self.rows, container_id)


    def locate(self, isbn: str) -> str:

        return locate_the_book(self.rows, isbn, "")
