"""RetrieveBehavior class"""


from typing import List
from library.item.book import Book


class RetrieveBehavior:
    """Informal RetrieveBehavior interface"""


    def retrieve_books(self, container_id: str) -> List[Book]:
        """retrieves books from own container with given id"""


    def locate(self, isbn: str) -> str:
        """gives the id of the container where the book with the given isbn is,
        returns empty string if not found"""
