"""Shelf class"""

from typing import List
from library.item.book import Book
from .container import Container
from .row import Row
from .helpers import locate_the_book, retrieve_containers_books


class Shelf(Container):
    """Represents a shelf in the library"""


    def __init__(self, given_id: str) -> None:
        super().__init__(given_id)
        self.rows: List[Row] = []


    def locate(self, isbn: str) -> str:

        return locate_the_book(self.rows, isbn, " -> shelf: ")


    def get_books(self) -> List[Book]:

        return retrieve_containers_books(self.rows)
