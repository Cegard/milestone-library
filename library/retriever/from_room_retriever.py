"""FromRoomRetriever class"""

from library.retriever.helpers import get_books, locate_the_book
from typing import List
from library.item.book import Book
from .retrieve_behavior import RetrieveBehavior
from .room import Room


class FromRoomRetriever(RetrieveBehavior):
    """implements the RretrieveBhavior 'interface' to look for books in rooms"""


    def __init__(self) -> None:
        super().__init__()
        self.rooms: List[Room] = []


    def retrieve_books(self, container_id: str) -> List[Book]:

        return get_books(self.rooms, container_id)


    def locate(self, isbn: str) -> str:
        route = locate_the_book(self.rooms, isbn, "")

        if route:
            route = route[1:]

        return route
