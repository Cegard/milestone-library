"""Row class"""

from typing import List
from library.item.book import Book
from .container import Container


class Row(Container):
    """Represents a shelf row"""


    def __init__(self, given_id: str) -> None:
        super().__init__(given_id)
        self.books: List[Book] = []


    def locate(self, isbn: str) -> str:
        route = ""

        if len([book for book in self.books if book.isbn == isbn]) > 0:
            route = " -> row: " + self.given_id

        return route


    def get_books(self) -> List[Book]:

        return self.books
