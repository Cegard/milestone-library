"""Room class"""

from typing import List
from library.item.book import Book
from .container import Container
from .helpers import locate_the_book, retrieve_containers_books


class Room(Container):
    """Represents a room in the library"""


    def __init__(self, given_id: str) -> None:
        super().__init__(given_id)
        self.shelfs: List[Container] = []


    def locate(self, isbn: str) -> str:

        return locate_the_book(self.shelfs, isbn, "room: ")


    def get_books(self) -> List[Book]:

        return retrieve_containers_books(self.shelfs)
