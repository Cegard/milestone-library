"""FromShelfRetriever class"""

from library.retriever.helpers import get_books, locate_the_book
from typing import List
from library.item.book import Book
from .retrieve_behavior import RetrieveBehavior
from .shelf import Shelf


class FromShelfRetriever(RetrieveBehavior):
    """implements the RretrieveBhavior 'interface' to look fro books on shelfs"""


    def __init__(self) -> None:
        super().__init__()
        self.shelfs: List[Shelf] = []


    def retrieve_books(self, container_id: str) -> List[Book]:

        return get_books(self.shelfs, container_id)


    def locate(self, isbn: str) -> str:

        return locate_the_book(self.shelfs, isbn, "")
