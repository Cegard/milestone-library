"""Container class"""

from typing import List
from library.item.book import Book


class Container:
    """"Represents a physical item container"""


    def __init__(self, given_id: str) -> None:
        self.given_id = given_id


    def locate(self, isbn: str) -> str:
        """returns the route to follow to find the book with the given isbn
        returns empty string in case not found
        """


    def get_books(self) -> List[Book]:
        """tells if the book with the given isbn is in this container"""
