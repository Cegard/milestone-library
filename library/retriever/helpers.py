"""utils for retrievers"""

from typing import List
from library.item.book import Book


def retrieve_containers_books(containers: list) -> List[Book]:
    """gets the books in the containers list"""
    books: List[Book] = []

    for container in containers:
        books += container.get_books()

    return books


def locate_the_book(containers: list, isbn: str, prefix: str) -> str:
    """returns the route indications to find the book with the given isbn"""
    route = ""
    was_found = False
    containers_count = len(containers)
    index = 0

    while index <  containers_count and not was_found:
        possible_route = containers[index].locate(isbn)
        was_found = possible_route != ""

        if was_found:
            route = prefix + containers[index].given_id + possible_route

        index += 1

    return route


def get_books(containers: list, container_id: str) -> List[Book]:
    """gets the books in the given containers list"""
    books: List[Book] = []
    was_found = False
    number_of_rows = len(containers)
    index = 0

    while index < number_of_rows and not was_found:
        was_found = containers[index].given_id == container_id

        if was_found:
            books += containers[index].get_books()

        index += 1

    return books
