"""utils for inentory"""

from typing import List
from library.utils.constants import wildcards, comparatorsKeys
from library.item.book import Book


def expressions_matches(book: Book, exprs: List[str]) -> bool:
    """checks if all expressions matches at least one rule on the given book's attrs"""
    result = True
    index = 0
    exprs_len = len(exprs)

    # stops earlier than python's built in reducer
    while index < exprs_len and result:
        result &= matches_equals(book, exprs[index]) or \
            matches_start(book, exprs[index]) or \
            matches_ends(book, exprs[index]) or \
            matches_has(book, exprs[index])
        index += 1

    return result


def does_match(book: Book, pattern: str, rule: str) -> bool:
    """checks if in some of the fields of the given book
    theres's a match of the the given rule
    """

    return comparators[rule](book.title, pattern) or \
        comparators[rule](book.publisher, pattern) or \
        comparators[rule](str(book.published_year), pattern) or \
        (sum(1 for author in book.authors if comparators[rule](author, pattern)) > 0)

matches_equals = lambda book, exp: \
    wildcards.ANY not in exp and does_match(book, exp, comparatorsKeys.EQUAL)

matches_start = lambda book, exp: \
    exp[-1] == wildcards.ANY and does_match(book, exp[0:-2], comparatorsKeys.STARTS)

matches_ends = lambda book, exp: \
    wildcards.ANY == exp[0]  and does_match(book, exp[1:], comparatorsKeys.ENDS)

matches_has = lambda book, exp: \
    wildcards.ANY == exp[0] and wildcards.ANY == exp[-1] and \
    does_match(book, exp[1:-2], comparatorsKeys.HAS)


comparators = {
    comparatorsKeys.EQUAL: lambda x, y: x == y,
    comparatorsKeys.STARTS: lambda string, prefix: string.lower().startswith(prefix.lower()),
    comparatorsKeys.ENDS: lambda string, sufix: string.lower().endswith(sufix.lower()),
    comparatorsKeys.HAS: lambda string, sub: string.lower().find(sub.lower()) > -1
}
