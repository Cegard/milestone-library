"""Inventory class"""

from typing import List, Dict
from library.item.physical import Physical
from library.item.book import Book
from library.retriever.retrieve_behavior import RetrieveBehavior
from library.utils.constants import wildcards
from .helpers import expressions_matches


class Inventory:
    """represents the collection of physical items in the library"""


    def __init__(self) -> None:
        self.items: List[Physical] = []
        self.retrievers: Dict[str, RetrieveBehavior] = dict()


    def __find_books_by_and(self, search_string: str) -> List[Book]:
        """finds the books that match the given search string"""
        found_books = []
        patterns = search_string.split(wildcards.AND)

        for item in self.items:

            if isinstance(item, Book):
                book: Book = item

                if expressions_matches(book, patterns):
                    found_books.append(book)

        return found_books


    def find_books(self, search_string: str) -> List[Book]:
        """finds the books that match the given search string"""
        found_books = set()
        expressions = search_string.split(wildcards.OR)

        for expr in expressions:
            books = self.__find_books_by_and(expr)

            for book in books:
                found_books.add(book)

        return list(found_books)


    def get_books_from(self, container: str, container_id: str) -> List[Book]:
        """gets the books in the container with the given id"""
        books: List[Book] = []
        retriever = self.retrievers.get(container, None)

        if retriever:
            books = retriever.retrieve_books(container_id)

        return books


    def locate(self,isbn: str) -> str:
        """returns the route to follow to find the book with the given isbn"""
        return  self.retrievers["room"].locate(isbn)
