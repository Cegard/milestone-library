"""constants"""

from collections import namedtuple


Wildcards = namedtuple(
    "Wildcards",
    [
        "ANY",
        "AND",
        "OR"
    ]
)

wildcards = Wildcards("*", " & ", " | ")


ComparatorsNames = namedtuple(
    "CmparatorsNames",
    [
        "EQUAL",
        "STARTS",
        "ENDS",
        "HAS"
    ]
)


comparatorsKeys = ComparatorsNames(
    "equal",
    "starts_with",
    "ends_with",
    "has"
)