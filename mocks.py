# -*- coding: utf-8 -*-
"""Contains mocks for testing purpuses"""

from library.item.book import Book
from library.retriever.row import Row
from library.retriever.shelf import Shelf
from library.retriever.room import Room
from library.retriever.from_row_retriever import FromRowRetriever
from library.retriever.from_shelf_retriever import FromShelfRetriever
from library.retriever.from_room_retriever import FromRoomRetriever
from library.inventory.inventory import Inventory

inventory = Inventory()

row_a = Row("0")
row_b = Row("1")

shelf_a = Shelf("0")
shelf_b = Shelf("1")

room_a = Room("0")
room_b = Room("1")

row_retriever = FromRowRetriever()
shelf_retriever = FromShelfRetriever()
room_retriever = FromRoomRetriever()

book_a = Book("0")
book_a.title = "Texts from Denmark"
book_a.isbn = "xxx-xx-xxx-xx"
book_a.pages = 200
book_a.publisher = "Gyldendal"
book_a.published_year = 2001
book_a.authors.append("Brian Jensen")

book_b = Book("1")
book_b.title = "Stories from abroad"
book_b.isbn = "xxx-xx-xxx-xy"
book_b.pages = 200
book_b.publisher = "Borgen"
book_b.published_year = 2012
book_b.authors.append("Peter Jensen")
book_b.authors.append("Hans Andersen")

book_c = Book("2")
book_c.title = "El olvido que seremos"
book_c.isbn = "xxx-xx-xxx-xz"
book_c.pages = 200
book_c.publisher = "Planeta"
book_c.published_year = 2007
book_c.authors.append("Héctor Abad Faciolince")

inventory.items.append(book_a)
inventory.items.append(book_b)
inventory.items.append(book_c)

row_a.books.append(book_a)
row_b.books.append(book_b)
row_b.books.append(book_c)

shelf_a.rows.append(row_a)
shelf_b.rows.append(row_b)

room_a.shelfs.append(shelf_a)
room_b.shelfs.append(shelf_b)

row_retriever.rows.append(row_a)
row_retriever.rows.append(row_b)

shelf_retriever.shelfs.append(shelf_a)
shelf_retriever.shelfs.append(shelf_b)

room_retriever.rooms.append(room_a)
room_retriever.rooms.append(room_b)

inventory.retrievers["row"] = row_retriever
inventory.retrievers["shelf"] = shelf_retriever
inventory.retrievers["room"] = room_retriever
